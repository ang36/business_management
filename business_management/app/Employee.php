<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = [];

    public function companie(){
        return $this->belongsTo('App\Companie');
    }
}
