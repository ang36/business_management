<?php

namespace App\Http\Controllers\Admin;

use App\Companie;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CreateCompanieRequest;
use App\Http\Requests\EditCompanieRequest;
use App\Mail\MessageReceived;
use Illuminate\Support\Facades\Mail;

class CompanieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/record_companie',[
            'companies'=>Companie::first()->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/companie');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCompanieRequest $request)
    {
        $logo = $request->file('logo');
        $data = $request->validated();
        $data['logo'] =  $request->file('logo')->store('images');
        $data['name_logo'] =  $logo->getClientOriginalName();
        Companie::create($data);
        Mail::to(request('email'))->queue(new MessageReceived($data));
        return redirect()->route('companie.create')->with('status',__('successful company registration'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function show(Companie $companie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function edit(Companie $companie)
    {
        return view('admin/edit_companie',[
            'companie'=>$companie
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanieRequest $request, Companie $companie)
    {
        $data = $request->validated();
        if(request('logo')){
            $logo = $request->file('logo');
            $data['logo'] = $request->file('logo')->store('images');
            $data['name_logo'] = $logo->getClientOriginalName();
        }
        $companie->update($data);
        return redirect()->route('companie.index')->with('status',__('successfully upgraded company'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Companie  $companie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companie $companie)
    {
        $companie->delete();
        return redirect()->route('companie.index')->with('status',__('Company successfully eliminated'));
    }
}
