<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Companie;
use App\Employee;

class HomeController extends Controller
{
    public function index(){
        return view('admin/index',[
            'companies'=>Companie::count(),
            'employees'=>Employee::count()
        ]);
    }

}
