@extends('adminlte::page')

@section('content')
    @include('partials/session-status')
	<div class="row">
		<div class="col mt-3">
			<table class="table table-sm">
			  	<thead class="thead-dark">
				    <tr>
				      	<th scope="col">{{__('id')}}</th>
				      	<th scope="col">{{__('name')}}</th>
				      	<th scope="col">{{__('email')}}</th>
				      	<th scope="col">{{__('logo')}}</th>
				      	<th scope="col">{{__('website')}}</th>
				      	<th scope="col">{{__('Edit')}}</th>
				      	<th scope="col">{{__('Delete')}}</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@foreach($companies as $companie)
				  		<tr>
				      		<td>{{$companie->id}}</td>
				      		<td>{{$companie->name}}</td>
			      			<td>{{$companie->email}}</td>
				      		<td><img src="http://localhost:8000/storage/{{$companie->logo}}" alt=""  width="20" height="20"></td>
				      		<td>{{$companie->website}}</td>
				      		<td><a  class="btn btn-primary" role="button" href="{{route('companie.edit', $companie)}}">{{__('Edit')}}</a></td>
				      		<td>
				      			<form method="POST" action="{{route('companie.destroy',$companie)}}">
				      				@csrf @method('DELETE')
				      				<button class="btn btn-primary">{{__('Delete')}}</button>
				      			</form>
				      		</td>

				    	</tr>
			  		@endforeach
			  	</tbody>
			</table>
	  		{{$companies->links()}}
	  	</div>
  	</div>
@stop