@extends('adminlte::page')

@section('content')
	@include('partials/session-status')
	<div class="row">
		<div class="col mt-3">
			<table class="table table-sm">
			  	<thead class="thead-dark">
				    <tr>
				      	<th scope="col">{{__('id')}}</th>
				      	<th scope="col">{{__('name')}}</th>
				      	<th scope="col">{{__('surname')}}</th>
				      	<th scope="col">{{__('email')}}</th>
				      	<th scope="col">{{__('phone')}}</th>
				      	<th scope="col">{{__('companie_id')}}</th>
				      	<th scope="col">{{__('Edit')}}</th>
				      	<th scope="col">{{__('Delete')}}</th>
				    </tr>
			  	</thead>
			  	<tbody>
			  		@foreach($employees as $employee)
				    	<tr>
				      		<td>{{$employee->id}}</td>
			      			<td>{{$employee->name}}</td>
				      		<td>{{$employee->surname}}</td>
				      		<td>{{$employee->email}}</td>
				      		<td>{{$employee->phone}}</td>
				      		<td>{{$employee->companie_id}}</td>
				      		<td><a class="btn btn-primary" role="button"href="{{route('employee.edit',$employee)}}">{{__('Edit')}}</a></td>
				      		<td>
				      			<form method="POST" action="{{route('employee.destroy',$employee)}}">
				      				@csrf @method('DELETE')
				      				<button class="btn btn-primary">{{__('Delete')}}</button>
				      			</form>
				      		</td>
				    	</tr>
				    @endforeach
			  </tbody>
			</table>
			{{$employees->links()}}
		</div>
	</div>
@stop