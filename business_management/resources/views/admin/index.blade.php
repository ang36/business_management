@extends('adminlte::page')

@section('content')
	<div class="row">
		<div class="col-lg-3 col-6 mt-3">

			<div class="small-box bg-info">
				<div class="inner">
					<h3>{{$companies}}</h3>
					<p>{{__('Companies')}}</p>
				</div>
				<a href="{{route('companie.index')}}" class="small-box-footer">{{__('More info')}}<i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-6 mt-3">

			<div class="small-box bg-warning">
				<div class="inner">
					<h3>{{$employees}}</h3>
					<p>{{__('Employees')}}</p>
				</div>
				<a href="{{route('employee.index')}}" class="small-box-footer">{{__('More info')}}<i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
	</div>
@stop