@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card mt-3">
                    <div class="card-header text-center">{{ __('Edit Companie') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{route('companie.update',$companie)}}" enctype="multipart/form-data">
                            @csrf @method('PUT')

                            <div class="form-group row">
                                <label for="logo" class="col-md-2 col-form-label text-md-right">{{ __('Logo') }}</label>

                                <div class="col-md-9">
                                    <input id="logo" type="file"  name="logo" autocomplete="logo">
                                    <a href="http://localhost:8000/storage/{{$companie->logo}}">{{$companie->name_logo}}</a>

                                    @error('logo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-9">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name',$companie->name) }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('Email') }}</label>

                                <div class="col-md-9">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email',$companie->email) }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="website" class="col-md-2 col-form-label text-md-right">{{ __('Website') }}</label>

                                <div class="col-md-9">
                                    <input id="website" type="text" class="form-control" name="website" value="{{ old('website',$companie->website) }}" required autocomplete="website">
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary w-100">
                                        {{ __('Edit') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop