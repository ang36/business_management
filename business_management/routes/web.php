<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\Admin\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes(['register'=>false]);

Route::prefix('admin')->group(function () {
    Route::get('/','Admin\HomeController@index')
    ->name('admin.index')
    ->middleware('auth');

    Route::resource('companie','Admin\CompanieController')->middleware('auth');
    Route::resource('employee','Admin\EmployeeController')->middleware('auth');
});