<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleUser::class);
        $user = factory(App\User::class, 1)->create();
        foreach($user as $users){
            $users->assignRole('Admin');
        }
        // $user->assignRole('Admin');
    }
}
